package com.company.boss;

import com.company.game.GameEntity;

public class Boss extends GameEntity {

    private int previosDamage;
    public Boss(int health, int damage){
        super(health, damage);
    }


    public int getPreviosDamage() {
        return previosDamage;
    }

    public void setPreviosDamage(int previosDamage) {
        this.previosDamage = previosDamage;
    }
}
