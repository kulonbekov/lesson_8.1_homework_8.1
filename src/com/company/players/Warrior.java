package com.company.players;

import com.company.boss.Boss;
import com.company.game.Ability;

public class Warrior extends Hero {

    public Warrior(int health, int damage) {
        super(health, damage, Ability.SAVE_DAMAGE_AND_REVERT);
    }

    @Override
    public void useAbility(Hero[] heroes, Boss boss) {
        for (Hero hero : heroes) {
            if (hero.getHealth() > 0) {
                hero.setDamage(hero.getDamage() + boss.getDamage());
            }
        }
    }
}
