package com.company.players;

import com.company.boss.Boss;
import com.company.game.Ability;
import com.company.game.RpgGame;

import java.util.Random;

public class Magical extends Hero {


    Random r = new Random();
    int randomNumber = r.nextInt(1) + 1;

    public Magical(int health, int damage) {

        super(health, damage, Ability.BOOST);
    }

    @Override
    public void useAbility(Hero[] heroes, Boss boss) {

        for (Hero hero : heroes) {
            if (hero.getHealth() > 0) {
                for (int i = 0; i < heroes.length; i++) {

                    heroes[i].setDamage(heroes[i].getDamage() * randomNumber);
                }

            }

        }
    }

}
