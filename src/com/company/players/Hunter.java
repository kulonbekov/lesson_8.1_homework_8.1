package com.company.players;

import com.company.boss.Boss;
import com.company.game.Ability;

import java.util.Random;

public class Hunter extends Hero {
    Random r = new Random();
    int randomHunter = r.nextInt(2) + 2;

    public Hunter(int health, int damage) {
        super(health, damage, Ability.CRITICAL_DAMAGE);
    }

    @Override
    public void useAbility(Hero[] heroes, Boss boss) {
        for (Hero hero : heroes) {
            if (hero.getHealth() > 0) {

                hero.setDamage(hero.getDamage() * randomHunter);
            }
        }
    }
}

