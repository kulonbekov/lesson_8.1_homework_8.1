package com.company.players;

import com.company.boss.Boss;
import com.company.game.Ability;

import java.util.Random;

public class Thor extends Hero {


    public Thor(int health, int damage) {
        super(health, damage, Ability.CRITICAL_DAMAGE);
    }

    @Override
    public void useAbility(Hero[] heroes, Boss boss) {

        Random r = new Random();
        int randomHunter = r.nextInt(1);


        if (randomHunter == 1) {
            System.out.println("Тор применил супер удар");
            boss.setPreviosDamage(boss.getDamage());
            boss.setDamage(0);
        } else {
            boss.setDamage(boss.getPreviosDamage());
        }
    }
}

